var express = require('express');
var router = express.Router();

const google = require('googleapis');
const scopes = ['https://www.googleapis.com/auth/spreadsheets'];

const allowedApiKeys = require('../.credentials/apikeys.json');
const gsheetConfig = require('../configs/gsheet.json');
const spreadsheetId = gsheetConfig.spreadsheetId;
const jwtKey = require(gsheetConfig.authFile);
const jwtClient = new google.auth.JWT(jwtKey.client_email, null, jwtKey.private_key, scopes, null);
let auth;

jwtClient.authorize((err, tokens) => {
  if (err) console.log(err);
  auth = jwtClient;
});

app.post('/', (req, res) => {
  if (!allowedApiKeys.includes(req.body.apiKey)){
    res.status(400).send('Bad Api Key');
    return false;
  }



});

/* GET home page. */
router.get('/', function(req, res){
  var scope = {
    data: {
      title: "Main page",
      text: "Opa"
    },
    vue: {
      head: {
        title: "Main"
      },
      components: ['list']
    }
  };
  res.render('index', scope);
});

module.exports = router;
